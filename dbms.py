#!/usr/bin/env python2


# file dbms.py
# Last Modified: nov 14 2018

## brief Database Management System Version 1
# details DBMS for CS457
#
# version 1.00 
#         Ignacio Chamorro(22 September 2018)
#              Original Implementation
#
# version 1.01 
#         Ignacio Chamorro(27 September 2018)
#              Original Implementation Continued
#
# version 2.00
#         Ignacio Chamorro(13 October 2018)
#              Implemented insert operation
#                 - extractValues() will get values for insert operation
#                 - pi() implemented
#                 - sigma() implemented
#                 - dropTable() implemented
#
# version 2.01
#         Ignacio Chamorro(18 October 2018)
#              Implemented update, delete, select
#              Added rules to get user imput for insert, update, delete and select
#                 - for more details look at the readme file
#
# version 3.00
#         Ignacio Chamorro(12 November 2018)
#              Implemented cartesian product, inner join, and left outer join
#              Fixed bugs:
#                 - empty input crashed dbms
#                 - sometimes creating a table and adding things crashed dbms
#                 - removed debuging output
#
# version 3.01
#         Ignacio Chamorro(14 November 2018)
#              Implemented the user interface to support joins 
#                 - see notes section for limitations on joins of this dbms
#              Added ackoloegment/feedback for insert update delete
#
# version 4.00
#         Ignacio Chamorro(08 December 2018)
#              Implemented transactions
#
# architecture:
#     - all the databases will be created in the working dorectory where the dbms was launched from
#     - each db will be its own folder in the working directory
#     - all the information of the db will be stored in side the db folder in the file ".db"
#     - first value of the .db file its the database real name, the way the user inputed
#     - all the tables will be stored in the database folder
#     - the .table_name files will contain table schema
#     - the table_name files will contain the records of the table
#     - this DBMS uses imediate materialization (sometimes conbined with memory) to do all editing to tables
# Notes: 
#     - this python script should be run with Pyhton 2.7 on linux only
#     - the script was made for ubuntu 18.04, should work on all linux
#     - the delimiter used is "|||", this allows a user to use | and || in a string
#     - since sometimes a line or files ends with ||| the split function will think
#        that there is another token after the delimiter when there is not 
#        so I have to check for it and delete it, otherwise i have empty tokens 
#        or python thinks index is out of bounds when accesinf the list of tokens 
#     - this dbms does not support nested queires    
#     - this dbms does only support one comparison on the select statement (where)
#           due to parser restrictions
#
#
# next version:
#     -select will be its own fundtion that will break down the query, for now it only can do *
#     -type checking when input is entered
#     -add function that removes empty tokens of the tokends list



import os # to move a file
import sys
import shutil # this tool will delete a non empty folder and copy files
import random 
import string
import glob

strDelimiter = "|||"
tableNotValid = "|$%^**NULLTABLE**^%$|"


# will allow to output text in a simillar maner as cout in c++
def stdout(string):
   sys.stdout.write(string)

def iniTempDB():#initialize tmporary database when one is not selected
   if os.path.exists(".TEMP_DB/.db"):
      shutil.rmtree(".TEMP_DB") # this tool will delete a non empty folder
   elif os.path.exists(".TEMP_DB"):
      print("ERROR: fail to create temporary database, make sure the is no folder with the name \".TEMP_DB\" in working directory")
      sys.exit()
   os.mkdir(".TEMP_DB")
   tempDBFile = open(".TEMP_DB/.db","a")#creating the file
   tempDBFile.write(".TEMP_DB"+strDelimiter)
   tempDBFile.close()

def selectAllTable(tableName):#will select all the table
   if os.path.exists(currentDB+"/."+tableName.upper()):
      tableInfo = open(currentDB+"/."+tableName.upper(), "r")
      info = (tableInfo.read()).split(strDelimiter)
      numbTokens = len(info)
      #print("len is ")
      if info[numbTokens-1] == '':  
         numbTokens = numbTokens-1 #since the file ends in ||| then it things there an extra one so I fix it here
      #print(numbTokens)
      index = 0
      while(index < numbTokens):
         #print index
         stdout(info[index]+" ")#printing name
         index = index + 1
         if "CHAR" in info[index]:
            stdout(info[index]+"("+info[index+1]+") ")
            index = index + 2
         else:
            stdout(info[index]+" ")
            index = index + 1
         if index < numbTokens:
            stdout("| ")
      tableInfo.close()
      print "" # do not remove this
      recordsFile = open(currentDB+"/"+tableName.upper(),"r")
      records = recordsFile.readlines()#this gives a list of lines
      for line in records:
         temp = line.split(strDelimiter)
         tempIndex = 0
         for item in temp:
            if tempIndex !=0:
               stdout(" | ")
            stdout(item)
            tempIndex =tempIndex+1      
      return True
   else:
      return False

 
def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
def isFloat(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False
def padChar(str, leng):
   if len(str) >= leng:
      return str
   else:
      return str.ljust(leng-len(str), ' ')
def unpadChar(str):
   index = len(str) -1
   while index >= 0:
      if not (str[index] == ' '):
         break
      index = index -1
   if index == -1:
      return ''
   return str[0:index+1]#seconds index determines the first charcter i dont want

def extractInfoElements(listOfItems):# this function will extract elements like attribute operand and value
   whereWho = ""
   whereOperand = ""
   whereValue = ""
   operands = {'=','!','>','<'}
   if len(listOfItems) == 0:
      return whereWho, whereOperand, whereValue
   buff = ""
   for item in listOfItems:#make list into one item 
      buff = buff+item
   index = 0
   while index < len(buff) and (buff[index] not in operands) :
      whereWho = whereWho+buff[index]
      index = index+1
   while index < len(buff) and (buff[index] in operands):
      whereOperand = whereOperand+buff[index]
      index = index+1
   while index < len(buff) and (buff[index] not in operands):
      whereValue = whereValue+buff[index]
      index = index+1
   whereWho = whereWho.replace(' ',"")
   whereOperand = whereOperand.replace(' ',"")
   whereValue = whereValue.replace('"',"")
   whereValue = whereValue.replace("'","")
   return whereWho, whereOperand, whereValue

def getAttributeIndexAndType(attributeName, table):
   #return the index and type of the attribute in table
   attributeIndex = -1
   attributeType = ""
   attributeCharNumber = ""
   tempIndex = 0
   if os.path.exists(currentDB+"/."+table.upper()):
      tableInfo = open(currentDB+"/."+table.upper(), "r")
      info = (tableInfo.read()).split(strDelimiter)
      numbTokens = len(info)
      if info[numbTokens-1] == '':  
         numbTokens = numbTokens-1 #since the file ends in ||| then it things there an extra one so I fix it here
      index = 0
      tempName = ""
      tempType = ""
      tempCharNumber = ""
      while(index < numbTokens):
         tempName = info[index]
         index = index + 1
         if "CHAR" in info[index]:
            tempType = info[index]
            tempCharNumber = info[index+1]
            index = index + 2
         else:
            tempType = info[index]
            index = index + 1
         if attributeName.upper() == tempName.upper():
            attributeIndex = tempIndex
            attributeType = tempType
            attributeCharNumber = tempCharNumber
            tableInfo.close()
            return attributeIndex, attributeType, tempCharNumber
         tempIndex = tempIndex +1
      tableInfo.close()
   return attributeIndex, attributeType, tempCharNumber




# def selectTable(fromTable,selectWhat="*", where=""):
#    print "selecting "+ selectWhat
#    print "from table "+fromTable
#    print "WHERE "+where

#    whereWho, whereOperand, whereValueType, whereValue = where(where)
#    if whereValueType == "NULL":
#       print "no where"


# def splitAtComma(str):
#    print "in "+str
#    buffList = []
#    buff = ""
#    index = 0 
#    while index < len(str):
#       print str[index]
#       if str[index] == ',':
#          print "found comma"
#       if str[index] == ',':
#          buffList = buffList.append(buff)
#          print "appending "+buff
#          buff =""
#       else:
#          buff = buff+str[index]
#       index = index+1
#    return buffList
def extractValues(listTokens):#extract values from the insert, tokens will not be split correctly (this will fix them), specially CHAR/VARCHAR types since th tokenized use space as delimiter
   buff = ""
   for token in listTokens: #return to one string
      buff = buff + ' ' +token
   while buff[0] == ' ':
      buff = buff[1:]#remove leading spaces
   buff = buff[7:]   #remove the values from in front
   buff = buff[0:len(buff)-1]# remove the ) at the end
   # we should have something, something , something  so lets tokenize with 
   #buff = buff.replace(',',"|||")
   #print buff

   buff = buff.split() 
   buffList = []
   for item in buff: #final sanitize, int/float remove spaces, char/varchar remove quotes and paces before and after the quotes
      #print "item: \"" + item+"\""
      if isInt(item) or isFloat(item):
         item = item.replace(' ','')#remove " "
         buffList.append(item)
      elif item.count("'") >= 2  or item.count('"') >= 2: #if string
         #remove all the spaces before the initial quote and the quote, and do the same from the other  end
         #print "string"
         index = 0 
         while index < len(item) :
            if item[index] == '"' or item[index] == "'":
               break
            index = index+1
         item = item[index+1:]
         index = len(item) -1
         while index >= 0 :
            if item[index] == '"' or item[index] == "'":
               break
            index = index-1
         item = item[:index]
         buffList.append(item)

      else:
         return False, buffList
   return True, buffList






def insert(table, valuesList):#only error check for the int and float done here
   #check if correct type before inserting

   if not os.path.exists(currentDB+"/."+table.upper()):
      print "Can not insert: Table does not exist"
      return False
   tableInfo = open(currentDB+"/."+table.upper(), "r")
   info = (tableInfo.read()).split(strDelimiter)
   numbTokens = len(info)
   #print("len is ")
   if info[numbTokens-1] == '':  
      numbTokens = numbTokens-1 #since the file ends in ||| then it thinks there an extra one so I fix it here
   #print(numbTokens)
   valuesListIndex = 0
   index = 0
   buff = ""
   valid = True

   #print len(valuesList)
   while(index < numbTokens):
      #print valuesListIndex
      if valuesListIndex>= len(valuesList):
         print "Cant insert, too few items given"
         return False
      index = index + 1 #skip name, we dont need it 
      if "CHAR" in info[index]:
         length = info[index+1]
         if "VAR" in info[index]:
            buff = buff+strDelimiter+valuesList[valuesListIndex]
         else:
            buff = buff+strDelimiter+padChar(valuesList[valuesListIndex],length)
         index = index + 2
         valuesListIndex = valuesListIndex+1
      elif info[index] == "INT":
         if isInt(valuesList[valuesListIndex]):
            buff = buff+strDelimiter+valuesList[valuesListIndex]
            valuesListIndex = valuesListIndex+1
         else:
            print "cant insert: "+valuesList[valuesListIndex]+" is not INT type"
            valid = False
            break
         index = index + 1
      elif info[index] == "FLOAT":
         #print valuesList
         #print valuesListIndex
         if isFloat(valuesList[valuesListIndex]):
            buff = buff+strDelimiter+valuesList[valuesListIndex]
            valuesListIndex = valuesListIndex+1
         else:
            print "cant insert: "+valuesList[valuesListIndex]+" is not FLOAT type"
            valid = False
            break
         index = index + 1
      else:
         print("ERROR: error in insert, due to reading not valid type from info file, developer error")
         sys.exit()
   tableInfo.close()
   if not valid:
      return False
   buff = buff[3:] # remove the ||| that gets accidentally added at the beginning in the previous step
   if len(valuesList) > valuesListIndex:
      print "Cant insert, too many items given"
      return False
   elif valuesListIndex > len(valuesList):
      print "Cant insert, too few items given"

   #writing data do file 
   table = open(currentDB+"/"+table.upper(),"a")
   table.write(buff+'\n')
   table.close()
   print "Inserted"
   return True

def newRandomTableName():
   prefix = "TEMP_"
   randomPart = ''.join(random.choice(string.ascii_uppercase) for _ in range(7))
   while os.path.exists(currentDB+"/."+prefix+randomPart):
      randomPart = ''.join(random.choice(string.ascii_uppercase) for _ in range(7))
   return prefix+randomPart

def dropTable(table):#will not verify if it exist, so make sure it does
   os.remove(currentDB+"/."+table.upper())
   os.remove(currentDB+'/'+table.upper())

def pi(table, listofAtributes, delete = False):# delete table passed in
   indexes = []
   types = []
   charNumber = []
   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   newSchema = ""
   for  item in listofAtributes:#note very efficient but does the job
      tempIndex, tempType, tempCharNumber = getAttributeIndexAndType(item,table)
      indexes.append(tempIndex)
      types.append(tempType)
      newSchema = newSchema +item.upper()+strDelimiter+tempType+strDelimiter 
      if "CHAR" in tempType:
         charNumber.append(tempCharNumber)
         newSchema = newSchema +tempCharNumber+strDelimiter
      else:
         charNumber.append("")


   info = open(currentDB+"/."+newTableName.upper(),"a")#creating table schema file
   info.write(newSchema)
   info.close()

   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file file
   oldRecordFile = open(currentDB+"/"+table.upper(),"r")#reading old records
   oldRecords = oldRecordFile.readlines()
   for rec in oldRecords:
      tempRecord = rec.replace('\n', '')
      recordInList = tempRecord.split(strDelimiter)
      tempRecord = ""
      index = 0 #index in the record list.
      attribIndex = 0 #number of the attribute in the old table
      insertedIndex = 0 # index of the newly inserted value
      #print recordInList
      #print len(recordInList)
      while index < len(recordInList):
         #print index
         value  = recordInList[index]
         index  = index + 1
         if attribIndex in indexes:
            tempRecord = tempRecord+ strDelimiter+value
            #if "CHAR" in types[insertedIndex]:
               #tempRecord = tempRecord+strDelimiter+charNumner[insertedIndex]
            insertedIndex = insertedIndex+1
         attribIndex = attribIndex +1
      #print "temp pi r: "+tempRecord
      while tempRecord[0] == '|':# remove the ||| delimiter from the begining
         tempRecord = tempRecord[1:]#remove the first cahracter
      tableRecords.write(tempRecord+'\n')

   tableRecords.close()
   oldRecordFile.close()
   if delete:
      dropTable(table)#delete old table, the one that got passed in
   return newTableName

#will take two valies as strings, but will do type change and comparison, type checking has to be done before passing in, this will not catch errors thrown
def whereCheck(valueType, leftValue, operand, rightValue):#this will just assume that all the types are correcnt, if not checked before this could crash
   # if the type is CHAR it will assume that they are both already padded, will trat as nomal string just like VARCHAR
   if valueType == "INT":
      left = int(leftValue)
      right = int(rightValue)
   elif valueType == "FLOAT":
      left = float(leftValue)
      right = float(rightValue)
   else:#if CHAR or VARCHAR
      left = leftValue
      right = rightValue
   if operand == "=":
      return left == right
   elif operand == "!=":
      return not (left == right)
   elif operand == ">":
      return left > right
   elif operand == "<":
      return left < right
   elif operand == ">=":
      return left >= right
   elif operand == "<=":
      return left <= right
   else: 
      return False

#before passing the value to check in make sure is of the right type, same type as the value  that will be checked againt 
def sigma(table, attributeName, operand, checkValue, delete = False):# make sure whn calling this the table and the attribute exist
   index, valueType, charNumber = getAttributeIndexAndType(attributeName,table)
   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+table.upper(),currentDB+"/."+newTableName.upper())#copy schema to new table


   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   oldRecordFile = open(currentDB+"/"+table.upper(),"r")#reading old records
   oldRecords = oldRecordFile.readlines()
   for rec in oldRecords:# reading record by record
      tempRecord = rec.replace('\n', '')
      recordInList = tempRecord.split(strDelimiter)
      tempRecord = ""
      loopIndex = 0 #index in the record list.
      insertRecord = False
      for value in recordInList:
         if loopIndex == index: #if its the one we have to compare
            if whereCheck(valueType,value, operand, checkValue):
               insertRecord = True
         loopIndex = loopIndex +1

      if insertRecord:
         #print "temp sigma r: "+rec
         tableRecords.write(rec)

   tableRecords.close()
   oldRecordFile.close()
   if delete:
      dropTable(table)#delete old table, the one that got passed in
   return newTableName

#before passing the value to check in make sure is of the right type, same type as the value  that will be checked againt 
def updateTable(table, attributeName, operand, checkValue,attributeNameToSet, setValue, overwriteTable = False):# make sure whn calling this the table and the attribute exist
   index, valueType, charNumber = getAttributeIndexAndType(attributeName,table)
   indexToChange, valueTypeToChange, charNumberToChange = getAttributeIndexAndType(attributeNameToSet,table)
   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+table.upper(),currentDB+"/."+newTableName.upper())#copy schema to new table


   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   oldRecordFile = open(currentDB+"/"+table.upper(),"r")#reading old records
   oldRecords = oldRecordFile.readlines()
   for rec in oldRecords:# reading record by record
      tempRecord = rec.replace('\n', '')
      recordInList = tempRecord.split(strDelimiter)
      tempRecord = ""
      loopIndex = 0 #index in the record list.
      valueChanged = False
      for value in recordInList:
         if loopIndex == index and whereCheck(valueType,value, operand, checkValue):
            valueChanged = True
         if loopIndex == indexToChange: #if its the one we have to compare
            tempRecord = tempRecord+strDelimiter+ setValue
         else:#just insert the same value
            tempRecord = tempRecord+strDelimiter+ value
         loopIndex = loopIndex +1
      if valueChanged:
         while tempRecord[0] == "|":#remove the delimiter from the begining
            tempRecord =tempRecord[1:]
         #print "temp updated r: "+tempRecord
         tableRecords.write(tempRecord+'\n')
      else:
         #print "temp not updated r: "+rec
         tableRecords.write(rec)

   tableRecords.close()
   oldRecordFile.close()
   if overwriteTable:
      dropTable(table)#delete old table, the one that got passed in
      os.rename(currentDB+"/."+newTableName.upper(), currentDB+"/."+table.upper())
      os.rename(currentDB+"/"+newTableName.upper(), currentDB+"/"+table.upper())
      return table
   return newTableName

#before passing the value to check in make sure is of the right type, same type as the value  that will be checked againt 
def removeFrom(table, attributeName, operand, checkValue, overwriteTable = False):# make sure whn calling this the table and the attribute exist
   index, valueType, charNumber = getAttributeIndexAndType(attributeName,table)
   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+table.upper(),currentDB+"/."+newTableName.upper())#copy schema to new table


   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   oldRecordFile = open(currentDB+"/"+table.upper(),"r")#reading old records
   oldRecords = oldRecordFile.readlines()
   for rec in oldRecords:# reading record by record
      tempRecord = rec.replace('\n', '')
      recordInList = tempRecord.split(strDelimiter)
      tempRecord = ""
      loopIndex = 0 #index in the record list.
      keepRecord = True
      for value in recordInList:
         if loopIndex == index: #if its the one we have to compare
            if whereCheck(valueType,value, operand, checkValue):# if it matches then remove record
               keepRecord = False
         loopIndex = loopIndex +1
      if keepRecord:
         #print "temp kept r: "+rec
         tableRecords.write(rec)

   tableRecords.close()
   oldRecordFile.close()
   if overwriteTable:
      dropTable(table)#delete old table, the one that got passed in
      os.rename(currentDB+"/."+newTableName.upper(), currentDB+"/."+table.upper())
      os.rename(currentDB+"/"+newTableName.upper(), currentDB+"/"+table.upper())
      return table
   return newTableName

#before passing the value to check in make sure is of the right type, same type as the value  that will be checked againt 
def selectFrom(table,selectAtribList, attributeNameToCheck = "*", operand = "", checkValue = "", KeepResultTable = False):# make sure whn calling this the table and the attribute exist
   workingTable = table
   tablesToDrop = []
   if not (attributeNameToCheck == "*"):
      index, valueType, charNumber = getAttributeIndexAndType(attributeNameToCheck,table)
      newTableName = newRandomTableName()#garantee a new name temp name that is not used
      shutil.copyfile(currentDB+"/."+table.upper(),currentDB+"/."+newTableName.upper())#copy schema to new table


      tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
      oldRecordFile = open(currentDB+"/"+table.upper(),"r")#reading old records
      oldRecords = oldRecordFile.readlines()
      for rec in oldRecords:# reading record by record
         tempRecord = rec.replace('\n', '')
         recordInList = tempRecord.split(strDelimiter)
         tempRecord = ""
         loopIndex = 0 #index in the record list.
         keepRecord = False
         for value in recordInList:
            if loopIndex == index: #if its the one we have to compare
               if whereCheck(valueType,value, operand, checkValue):# if it matches then remove record
                  keepRecord = True
            loopIndex = loopIndex +1
         if keepRecord:
            # print "temp kept r: "+rec
            tableRecords.write(rec)
      tableRecords.close()
      oldRecordFile.close()
      workingTable =  newTableName
      if not  KeepResultTable:
         tablesToDrop.append(newTableName)

   if selectAtribList == "*":
      selectAllTable(workingTable)
   else:
      workingTable = pi(workingTable, attributeList)
      selectAllTable(workingTable)

   for tabl in tablesToDrop:
      dropTable(tabl)
   return workingTable


def anyOfListInString(lst,strg):
   currentState = False
   for item in lst:
      if item in strg:
         currentState = True
   return currentState




def cartesianProduct(table1, table2):#will return new table, will not check if tables exist
   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+table1.upper(),currentDB+"/."+newTableName.upper())#copy schema for new table
   table2Info = open(currentDB+"/."+table2.upper(),"r")#open 2nd table schema
   newtableInfo = open(currentDB+"/."+newTableName.upper(),"a")# open new table schema
   newtableInfo.write(table2Info.read())#append second schmema to new schema
   table2Info.close()
   newtableInfo.close()
   
   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   records1File = open(currentDB+"/"+table1.upper(),"r")#reading  records1
   records2File = open(currentDB+"/"+table2.upper(),"r")#reading  records2

   records1 = records1File.readlines()
   records2 = records2File.readlines()


   for rec1 in records1:# reading record by record
      tempRecord = rec1.replace('\n', strDelimiter)
      for rec2 in records2:
         tableRecords.write(tempRecord+rec2)
   records1File.close()
   records2File.close()
   tableRecords.close()

   return newTableName

def generateBlankRecord(table): #assumes table exists
   record = ""
   tableInfo = open(currentDB+"/."+table.upper(),"r")#open table schema
   schema = tableInfo.read()
   schema = schema.split(strDelimiter)
   numbTokens = len(schema)
   #print("len is ")
   if schema[numbTokens-1] == '':  
      numbTokens = numbTokens-1 #since the file ends in ||| then it things there an extra one so I fix it here
   #print(numbTokens)
   index = 0
   while(index < numbTokens):
      index = index + 1
      record = record+"NULL"
      if "CHAR" in schema[index]:
         index = index + 2
      else:
         index = index + 1
      if index < numbTokens:
         record = record+strDelimiter
   tableInfo.close()
   record = record+'\n'
   return record


def leftJoin(table1, table2, leftAtrib, rightAtrib):#will return new table, will not check if tables exist
   leftIndex, valueType1, charNumber1 = getAttributeIndexAndType(leftAtrib,table1)
   rightIndex, valueType2, charNumber2 = getAttributeIndexAndType(rightAtrib,table2)
   if valueType1 != valueType2:
      return tableNotValid

   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+table1.upper(),currentDB+"/."+newTableName.upper())#copy schema for new table
   table2Info = open(currentDB+"/."+table2.upper(),"r")#open 2nd table schema
   newtableInfo = open(currentDB+"/."+newTableName.upper(),"a")# open new table schema
   newtableInfo.write(table2Info.read())#append second schmema to new schema
   table2Info.close()
   newtableInfo.close()
   
   leftIndex, valueType, charNumber = getAttributeIndexAndType(leftAtrib,newTableName)
   rightIndex, valueType, charNumber = getAttributeIndexAndType(rightAtrib,newTableName)

   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   records1File = open(currentDB+"/"+table1.upper(),"r")#reading  records1
   records2File = open(currentDB+"/"+table2.upper(),"r")#reading  records2

   records1 = records1File.readlines()
   records2 = records2File.readlines()

   blankRightRecord = generateBlankRecord(table2)

   for rec1 in records1:# reading record by record
      matched = False
      tempRecord = rec1.replace('\n', strDelimiter)
      for rec2 in records2:
         tempRecord2 = tempRecord+rec2
         recordToks = tempRecord2.split(strDelimiter)
         if recordToks[leftIndex] == recordToks[rightIndex]:#if they are equal
            tableRecords.write(tempRecord+rec2)
            matched = True
      if not matched: 
         tableRecords.write(tempRecord+blankRightRecord)

   records1File.close()
   records2File.close()
   tableRecords.close()

   return newTableName

def innerJoin(table1, table2, leftAtrib, rightAtrib):#will return new table, will not check if tables exist
   leftIndex, valueType1, charNumber1 = getAttributeIndexAndType(leftAtrib,table1)
   rightIndex, valueType2, charNumber2 = getAttributeIndexAndType(rightAtrib,table2)
   if valueType1 != valueType2:
      return tableNotValid

   combinedTableName = cartesianProduct(table1, table2)

   newTableName = newRandomTableName()#garantee a new name temp name that is not used
   shutil.copyfile(currentDB+"/."+combinedTableName.upper(),currentDB+"/."+newTableName.upper())#copy schema for new table

   leftIndex, valueType, charNumber = getAttributeIndexAndType(leftAtrib,newTableName)
   rightIndex, valueType, charNumber = getAttributeIndexAndType(rightAtrib,newTableName)

   tableRecords = open(currentDB+"/"+newTableName.upper(),"a")#creating the new record file 
   combinedRecordsFile = open(currentDB+"/"+combinedTableName.upper(),"r")

   records = combinedRecordsFile.readlines()

   blankRightRecord = generateBlankRecord(table2)

   for rec in records:# reading record by record
      recordToks = rec.split(strDelimiter)
      if recordToks[leftIndex] == recordToks[rightIndex]:#if they are equal
         tableRecords.write(rec)

   tableRecords.close()
   dropTable(combinedTableName)

   return newTableName


def createDBLock(lockID):
      f = open(currentDB+"/.Lock_DB","a+")#create it 
      f.write(lockID+'\n');
      f.close()
def deleteDBLock():
   os.remove(currentDB+"/.Lock_DB")

def checkDBLockFile():
   return os.path.exists(currentDB+"/.Lock_DB")

def getDBLockOwner():
   f = open(currentDB+"/.Lock_DB","r")
   line = f.readline()
   f.close()
   return line
def myID():
   return str(os.getpid())

########################################################################
#####################                      #############################
#####################     Start of code    #############################
#####################                      #############################
########################################################################

currentDB = ".TEMP_DB"
inText = ""
inTransaction = False
committing = False
transactionQueue = []

iniTempDB()
commandDetected = True
exit = False
while(not exit):
   if not committing:
      if(not commandDetected): #get more input untill we get a ';'
         stdout('>')
         inText = inText +' '+ raw_input()
      else:# get input
         stdout("DBMS@")
         stdout(currentDB+": ")
         try:
            inText = raw_input()
         except(EOFError):
            exit = True
            #continue
         #stdout(inText+'\n')
      
      #check imput so far
      if inText == '':
         continue
      if inText == "--" or (inText[0] == '-' and inText[1] == '-'): # if we get a comment
         continue    
      if ';' in inText: # if we get a ; then its a command and we then have to remove it fornext stage
         inText = inText.replace(';','')
         inText = inText.replace(',',' ')
         commandDetected = True
         if inTransaction:
            tempTokens = inText.split()
            if tempTokens[0].upper() == "ABORT":
               if inTransaction:
                  inTransaction = False
                  committing = False
                  transactionQueue = []
                  if checkDBLockFile():# true if lock exists
                     if getDBLockOwner() == myID()+"\n":
                        deleteDBLock()
                        print "ABORTED"
               else:
                  print "nothing to ABORT"
               continue

            elif tempTokens[0].upper() == "COMMIT":
               if inTransaction:
                  inTransaction = False
                  if checkDBLockFile():# true if lock exists
                     if getDBLockOwner() == myID()+"\n":
                        deleteDBLock()
                        committing = True
                        print "Comitting"
                     else:
                        print "error: can not commit, DB locked, ABORTING"
                        committing = False
                        transactionQueue = []
               else:
                  print "error: cant commit, not in transaction"
               continue

            elif tempTokens[0].upper() != "SELECT":
               transactionQueue.append(inText)
               print "ok, will execute only if committed"
               continue
      else:# in we did not get ; then we just keep geting input
         commandDetected = False
         continue

   else:#if we are commiting
      if len(transactionQueue) > 0:
            inText = transactionQueue.pop(0)
      else:
         committing = False
         continue


   ## command procesing starts

   #stdout("procesing: "+ inText+'\n')
   tokens = inText.split()
   if len(tokens) == 0:
      continue
   command = (tokens.pop(0)).upper()

   if command == "EXIT":
      exit = True
      continue

   elif command == "--" or (command[0] == '-' and command[1] == '-'): # if we get a comment
      continue

   elif command == "BEGIN":
      if len(tokens) >= 1:
         if tokens[0].upper() == "TRANSACTION":
            # set transaction to true
            # write lock file and secret number
            if not inTransaction:
               inTransaction = True
               createDBLock(myID())
               transactionQueue = []
               print "Transaction Started"
            else:
               print "error: Already in transaction"
            continue
      else:
         print "BEGIN goes with transaction, otherwise meaningless"
         continue


   elif command == "ABORT":
      # abort():
      #set transaction to false
      #delete command list
      #unlock file if owner of file
      #set comiting to false
      if inTransaction:
         inTransaction = False
         committing = False
         transactionQueue = []
         if checkDBLockFile():# true if lock exists
            if getDBLockOwner() == myID()+"\n":
               deleteDBLock()
               print "ABORTED"
      else:
         print "nothing to ABORT"
      continue

   elif command == "COMMIT":

      # set transaction to false
      #if i locked file then commit run the changes 
      #        commit()
      #if not mine then abort()
      #
      #set comitting to true
      if inTransaction:
         inTransaction = False
         if checkDBLockFile():# true if lock exists
            if getDBLockOwner() == myID()+"\n":
               deleteDBLock()
               committing = True
               print "Comitting"
            else:
               print "error: can not commit, DB locked, ABORTING"
               committing = False
               transactionQueue = []
      else:
         print "error: cant commit, not in transaction"
      continue

   elif command == "CREATE":
      if len(tokens) < 2:
         print "CREATE needs more arguments"
         print len(tokens)
         for tok in tokens:
            print tok
      else:
         item = (tokens.pop(0)).upper()
         if item == "DATABASE":
            if len(tokens) == 0:
               print("CREATE DATABASE needs a name")
            else:
               #print "Creating db "+ tokens[0]
               if os.path.exists(tokens[0].upper()+'/.db'):
                  print(tokens[0]+" exists, can not create DB twice")
               elif os.path.exists(tokens[0].upper()+'/'):
                  print("Cant create db, make sure there is no file or folder with the name "+tokens[0].upper()+" in the working directory")
               else:
                  print("Database \""+tokens[0]+"\" created")
                  os.mkdir(tokens[0].upper())
                  dbInfo = open(tokens[0].upper()+"/.db","a")#creating the file
                  dbInfo.write(tokens[0]+strDelimiter)
                  dbInfo.close()

         elif item == "TABLE":
            if len(tokens) == 0:
               print("CREATE TABLE needs at least one more argument aka name")
            else:#code for creating a table
               #print tokens
               tableName = tokens.pop(0)
               if '(' in tableName:
                  tableName = tableName.replace('(', ' ', 1)# separate the name fomr the (
                  tableName, temp = tableName.split(" ", 1)
                  tokens.insert(0, temp)# this will insert what was accidentally glued to the name that belongs  to the variables name and types list
               #print "Creating table" + tableName
               if os.path.exists(currentDB+"/."+tableName.upper()):
                  print(tableName+" exists, can not create table twice")
               else:
                  print("table \""+tableName+"\" created")
                  tableInfo = open(currentDB+"/."+tableName.upper(),"a")#creating the file
                  tableRecords = open(currentDB+"/"+tableName.upper(),"w")
                  tableRecords.close()
                  #dbInfo = open(currentDB+"/.db","a")
                  #dbInfo.write(tableName+'\n')
                  #dbInfo.close()
                  #print tokens
                  for tok in tokens:
                     tok = tok.upper()
                     if "CHAR" in tok: # this takes care of the varchar parenthesis as well as the var parenthessis case
                        tok = tok.replace('(',' ')
                        tok = tok.replace(')',' ')
                        tok = tok.split()
                        tableInfo.write(tok[0]+strDelimiter)
                        tableInfo.write(tok[1]+strDelimiter)
                        continue
                     elif '(' in tok or ')' in tok: # any other case we just remove the parenthesis
                        tok = tok.replace('(','')
                        tok = tok.replace(')','')
                     tableInfo.write(tok.upper()+strDelimiter)
                  tableInfo.close()
         else:
            print("ERROR: can not CREATE invalid item "+item)
            continue
   
   elif command == "USE":
      if os.path.exists(tokens[0].upper()+'/.db'):
         stdout("Using "+tokens[0]+" database\n")
         currentDB = tokens[0].upper() 
      else:
         print "Fail to USE, database \""+tokens[0]+"\" does not exist"

   elif command == "DROP":
      if len(tokens) < 2:
         print "DROP needs more arguments"
      else:
         item = (tokens.pop(0)).upper()
         if item == "DATABASE":
            if len(tokens) == 0:
               print("DROP DATABASE needs a DB to DROP")
            else:
               if os.path.exists(tokens[0].upper()+'/.db'):
                  if currentDB == tokens[0].upper():
                     iniTempDB()
                     currentDB = ".TEMP_DB"
                  shutil.rmtree(tokens[0].upper())
                  print "Database \""+tokens[0]+"\" dropped"
               else:
                  print "Fail to DROP \""+tokens[0]+"\", DB does not exist"
         elif item == "TABLE":
            if len(tokens) == 0:
               print("CREATE TABLE needs a TABLE to DROP")
            else:
               if os.path.exists(currentDB+"/."+tokens[0].upper()):
                  os.remove(currentDB+"/."+tokens[0].upper())
                  os.remove(currentDB+'/'+tokens[0].upper())
                  #todo: add code that removes table from .db, once that is implemented in v2
                  print "Database \""+tokens[0]+"\" dropped"

               else:
                  print "Fail to DROP \""+tokens[0]+"\", TABLE does not exist"
      
   # elif command == "SELECT":
   #    if len(tokens) < 3:
   #       print "SELECT need more arguments, at least 3 (usage: select * FROM TABLE"
   #    else:
   #       if tokens[0] == '*':
   #          if not selectAllTable(tokens[2]):#will return if table exist and if it was able to select it. temporary format, next version will soport more things 
   #             print "Failed to query table "+tokens[2]+" because it does not exist"
   #       else:
   #          print "selective SELECT is not yet implemented, PA2 will have this implemented since it will have data"

   elif command == "ALTER":
      if len(tokens) < 5:
         print "ALTER needs at least two more arguments"
      else:
         item = (tokens.pop(0)).upper()
         if item == "TABLE":
            tableName = tokens.pop(0).upper()
            if os.path.exists(currentDB+"/."+tableName.upper()):
               tableInfo = open(currentDB+"/."+tableName.upper(),"a")
            else:
               print "can not ALTER non existant table"
               continue
            alterType = tokens.pop(0).upper()
            if alterType == "ADD":
               for tok in tokens:
                  tok = tok.upper()
                  if "CHAR" in tok: # this takes care of the varchar parenthesis as well as the var parenthessis case
                     tok = tok.replace('(',' ')
                     tok = tok.replace(')',' ')
                     tok = tok.split()
                     tableInfo.write(tok[0]+strDelimiter)
                     tableInfo.write(tok[1]+strDelimiter)
                  else:
                     tableInfo.write(tok+strDelimiter)
               tableInfo.close()
            else:
               print "operation "+ alterType +" of LATER not valid or not yet implemented"
               continue
            print "Table "+tableName+" modified"
         else:
            print "Item "+item+" can not be altered"


   elif command == "INSERT":
      if (tokens.pop(0)).upper() == "INTO":
         table = tokens.pop(0)
         if "VALUES(" not in tokens[0].upper() or ")" not in tokens[len(tokens)-1]:#if we have no values to add
            print "cant insert, no values given"
            continue
         valid, items = extractValues(tokens)
         if valid:
            #print "will insert: "
            #print items
            insert(table, items);
         else:
            print "cant insert: one or more values are of unknown type"
      else:
         print "error: INSERT missing INTO"
         continue


   elif command == "UPDATE":
      possibleCommands = ["SET", "WHERE"]
      setSeen = False
      whereSeen = False
      if len(tokens) < 5:
         print "UPDATE needs more arguments"
         continue
      else:
         tableName = (tokens.pop(0)).upper()
         if os.path.exists(currentDB+"/."+tableName.upper()):
            setInfo = []
            whereInfo = []
            while len(tokens) >= 1:#while not empty
               command = (tokens.pop(0)).upper()
               # print command
               # print tokens
               if command == "SET":
                  setSeen = True
                  while len(tokens) >=1 and (not anyOfListInString(possibleCommands, tokens[0].upper())) : #if not another command it must be part of the current commant
                     setInfo.append(tokens.pop(0))
                     #print setInfo
                  setAttrib, setOperand, setValue = extractInfoElements(setInfo)
               elif command == "WHERE":
                  whereSeen = True
                  while len(tokens) >=1 and (not anyOfListInString(possibleCommands, tokens[0].upper())): #if not another command it must be part of the current commant
                     whereInfo.append(tokens.pop(0))

                  whereAttrib, whereOperand, whereValue = extractInfoElements(whereInfo)
               else:
                  print "Cant UPDATE table "+tableName+ ", subcommand "+ command.upper()+ "not valid"
                  continue
            #after we extract everything then we call the resepective update function to carry out the operation
            #but first input chekcing 
            if not setSeen:
               print "Cant UPDATE, SET missing"
               continue
            if not whereSeen:
               print "Cant UPDATE, WHERE missing"
               continue
            attributeIndex, attributeType, charNumber = getAttributeIndexAndType(whereAttrib, tableName)
            # print attributeIndex
            # print attributeType
            # print charNumber

            if attributeIndex <= -1:
               print "cant UPDATE, attribute "+ whereAttrib.upper()+" not in table"
               continue
            if attributeType == "INT":
               if  not isInt(whereValue):
                  print "cant UPDATE table, in WHERE: INT expected"
                  continue
            elif attributeType == "FLOAT":
               if not isFloat(whereValue):
                  print "cant UPDATE table, in WHERE: FLOAT expected"
                  continue
            elif attributeType == "CHAR":
               whereValue = whereValue.replace("'",'')
               whereValue = whereValue.replace('"','')
               setValue = padChar(charNumber)
            elif attributeType == "VARCHAR":
               whereValue = whereValue.replace("'",'')
               whereValue = whereValue.replace('"','')
            else:   
               print "Cant UPDATE, value of the WHERE command is of unknown type"
               continue

            attributeIndex, attributeType, charNumber = getAttributeIndexAndType(setAttrib, tableName)
            # print attributeIndex
            # print attributeType
            # print charNumber
            if attributeIndex <= -1:
               print "cant UPDATE, attribute "+ setAttrib.upper()+" not in table"
               continue
            if attributeType == "INT":
               if not isInt(setValue):
                  print "cant UPDATE table, in WHERE: INT expected"
                  continue
            elif attributeType == "FLOAT":
               if not isFloat(setValue):
                  print "cant UPDATE table, in WHERE: FLOAT expected"
                  continue
            elif attributeType == "CHAR":
               setValue = setValue.replace("'",'')
               setValue = setValue.replace('"','')
               setValue = padChar(charNumber)
            elif attributeType == "VARCHAR":
               setValue = setValue.replace("'",'')
               setValue = setValue.replace('"','')
            else:
               print "Cant UPDATE, value of the SET command is of unknown type"
               continue
            updateTable(tableName, whereAttrib, whereOperand, whereValue, setAttrib, setValue, overwriteTable = True)
            print "Updated"
         else:
            print "Can not UPDATE table "+ tableName + ", it does not exist"
   
   elif command == "DELETE":
      if not ((tokens.pop(0)).upper() == "FROM"):
         print "Cant DELETE, missing FROM"
         continue
      possibleCommands = ["SET", "WHERE"]
      if len(tokens) < 3:
         print "Delete needs more arguments"
         continue
      else:
         tableName = (tokens.pop(0)).upper()
         if os.path.exists(currentDB+"/."+tableName.upper()):
            whereInfo = []
            command = (tokens.pop(0)).upper()
            if command == "WHERE":
               whereAttrib, whereOperand, whereValue = extractInfoElements(tokens)
            else:
               print "Cant DELETE from table "+tableName+ ", subcommand "+ command.upper()+ "not valid"
               continue
            #after we extract everything then we call the resepective update function to carry out the operation
            #but first input checking 

            attributeIndex, attributeType, charNumber = getAttributeIndexAndType(whereAttrib, tableName)

            if attributeIndex <= -1:
               print "cant DELETE, attribute "+ whereAttrib.upper()+" not in table"
               continue
            if attributeType == "INT":
               if  not isInt(whereValue):
                  print "cant DELETE from table, in WHERE: INT expected"
                  continue
            elif attributeType == "FLOAT":
               if not isFloat(whereValue):
                  print "cant DELETE from table, in WHERE: FLOAT expected"
                  continue
            elif attributeType == "CHAR":
               whereValue = whereValue.replace("'",'')
               whereValue = whereValue.replace('"','')
               setValue = padChar(charNumber)
            elif attributeType == "VARCHAR":
               whereValue = whereValue.replace("'",'')
               whereValue = whereValue.replace('"','')
            else:   
               print "Cant DELETE, value of the WHERE command is of unknown type"
               continue
            removeFrom(tableName, whereAttrib, whereOperand, whereValue, overwriteTable = True)
            print "Deleted"
         else:
            print "Can not DELETE table "+ tableName + ", it does not exist" 
   

   elif "LEFT" in (tok.upper() for tok in tokens) and "OUTER" in (tok.upper() for tok in tokens) and "JOIN" in (tok.upper() for tok in tokens):
      print "left join"
      if len(tokens) < 3:
         print "SELECT needs more arguments"
         continue
      else:
         whereSeen = False
         attributeList = []
         while len(tokens)>=1 and tokens[0].upper() != "FROM":
            attributeList.append((tokens.pop(0)).replace(',',''))
         if "*" in attributeList:
            attributeList = "*"

         isJoin = False
         tokens.pop(0)#remove the FROM
         #check if the we have table renaming
         #rename all variables in each table to have the table prefix
         #keep track of the who was changed to rename them back afterwards
         # do table join here 
         tableName = (tokens.pop(0)).upper()# get the first table name, we can have more ? lets check
         if tokens[0] != "WHERE":
            isJoin = True
         if os.path.exists(currentDB+"/."+tableName.upper()) :
            whereInfo = []
            if isJoin:
                  alias1 = tokens.pop(0).upper() #get first table alias 
                  tokens.pop(0)#remove keyword left
                  tokens.pop(0)#remove keyword outer
                  tokens.pop(0)#remoce keyowrd join               
                  tableName2  = tokens.pop(0).upper() # get table2 name
                  if not os.path.exists(currentDB+"/."+tableName2.upper()):
                     print "Can not SELECT table "+ tableName2 + ", it does not exist"
                     continue 
                  alias2 = tokens.pop(0).upper() #get  table2 alias


            if len(tokens) >=1:
               command = (tokens.pop(0)).upper()
               if command == "ON":
                  whereSeen = True
                  whereAttrib, whereOperand, whereValue = extractInfoElements(tokens)


            #after we extract everything then we call the resepective update function to carry out the operation
            #but first input checking 
            if whereSeen:
               attributeIndex, attributeType, charNumber = getAttributeIndexAndType(whereAttrib, tableName)

  #----->             #CHECK IF WE ARE DOING A JOING OR A REGULAR SELECT
               if isJoin:
                  orderReversed = False
                  attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                  if attributeIndex == -1: # the first operand was no in the first table 
                     orderReversed = True
                     attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias2+'.').upper() , ''), tableName2.upper())
                     if attributeIndex == -1:
                        print "can not find "+ whereAttrib+" in any privided table"
                        continue
                  if orderReversed:
                     attributeIndex2, attributeType2, charNumber2 = getAttributeIndexAndType((whereValue.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                     if attributeIndex2 == -1:
                        print "can not find "+whereValue+" in any provided table"
                  if orderReversed: #will use whereValue since that is the right hand
                     #the following will left join but first it will strip the table names of each 
                     outputTable = leftJoin(tableName,tableName2, (whereValue.upper()).replace((alias1+'.').upper() , ''), (whereAttrib.upper()).replace((alias2+'.').upper() , ''))
                  else: #will use whereValue since that is the right hand
                     #the following will left join but first it will strip the table names of each 
                     outputTable = leftJoin(tableName,tableName2, (whereAttrib.upper()).replace((alias1+'.').upper() , ''), (whereValue.upper()).replace((alias2+'.').upper() , ''))
                  selectAllTable(outputTable)

            elif isJoin:
               print "can not JOIN without a WHERE"
               continue

         else:
            print "Can not SELECT table "+ tableName + ", it does not exist" 
            continue

   elif "JOIN" in (tok.upper() for tok in tokens):
      print "inner join"
      if len(tokens) < 3:
         print "SELECT needs more arguments"
         continue
      else:
         whereSeen = False
         attributeList = []
         while len(tokens)>=1 and tokens[0].upper() != "FROM":
            attributeList.append((tokens.pop(0)).replace(',',''))
         if "*" in attributeList:
            attributeList = "*"

         isJoin = False
         tokens.pop(0)#remove the FROM
         #check if the we have table renaming
         #rename all variables in each table to have the table prefix
         #keep track of the who was changed to rename them back afterwards
         # do table join here 
         tableName = (tokens.pop(0)).upper()# get the first table name, we can have more ? lets check
         if tokens[0] != "WHERE":
            isJoin = True
         if os.path.exists(currentDB+"/."+tableName.upper()) :
            whereInfo = []
            if isJoin:
                  alias1 = tokens.pop(0).upper() #get first table alias
                  if  tokens[0].upper() == "INNER":
                     tokens.pop(0)#remoce keyowrd inner               
                  tokens.pop(0)#remoce keyowrd join               
                  tableName2  = tokens.pop(0).upper() # get table2 name
                  if not os.path.exists(currentDB+"/."+tableName2.upper()):
                     print "Can not SELECT table "+ tableName2 + ", it does not exist"
                     continue 
                  alias2 = tokens.pop(0).upper() #get  table2 alias


            if len(tokens) >=1:
               command = (tokens.pop(0)).upper()
               if command == "ON":
                  whereSeen = True
                  whereAttrib, whereOperand, whereValue = extractInfoElements(tokens)


            #after we extract everything then we call the resepective update function to carry out the operation
            #but first input checking 
            if whereSeen:
               attributeIndex, attributeType, charNumber = getAttributeIndexAndType(whereAttrib, tableName)

  #----->             #CHECK IF WE ARE DOING A JOING OR A REGULAR SELECT
               if isJoin:
                  orderReversed = False
                  attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                  if attributeIndex == -1: # the first operand was no in the first table 
                     orderReversed = True
                     attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias2+'.').upper() , ''), tableName2.upper())
                     if attributeIndex == -1:
                        print "can not find "+ whereAttrib+" in any privided table"
                        continue
                  if orderReversed:
                     attributeIndex2, attributeType2, charNumber2 = getAttributeIndexAndType((whereValue.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                     if attributeIndex2 == -1:
                        print "can not find "+whereValue+" in any provided table"
                  if orderReversed: #will use whereValue since that is the right hand
                     #the following will innerjoin but first it will strip the table names of each 
                     outputTable = innerJoin(tableName,tableName2, (whereValue.upper()).replace((alias1+'.').upper() , ''), (whereAttrib.upper()).replace((alias2+'.').upper() , ''))
                  else: #will use whereValue since that is the right hand
                     #the following will innerjoin but first it will strip the table names of each 
                     outputTable = innerJoin(tableName,tableName2, (whereAttrib.upper()).replace((alias1+'.').upper() , ''), (whereValue.upper()).replace((alias2+'.').upper() , ''))
                  selectAllTable(outputTable)

            elif isJoin:
               print "can not JOIN without a WHERE"
               continue

         else:
            print "Can not SELECT table "+ tableName + ", it does not exist" 
            continue



   elif command == "SELECT":
      print "in select"
      if len(tokens) < 3:
         print "SELECT needs more arguments"
         continue
      else:
         whereSeen = False
         attributeList = []
         while len(tokens)>=1 and tokens[0].upper() != "FROM":
            attributeList.append((tokens.pop(0)).replace(',',''))
         if "*" in attributeList:
            attributeList = "*"

         isJoin = False
         tokens.pop(0)#remove the FROM
         #check if the we have table renaming
         #rename all variables in each table to have the table prefix
         #keep track of the who was changed to rename them back afterwards
         # do table join here 
         tableName = (tokens.pop(0)).upper()# get the first table name, we can have more ? lets check
         if len(tokens)>0 and tokens[0].upper() != "WHERE":
            isJoin = True
         if os.path.exists(currentDB+"/."+tableName.upper()) :
            whereInfo = []
            if isJoin:
                  alias1 = tokens.pop(0).upper() #get first table alias                
                  tableName2  = tokens.pop(0).upper() # get table2 name
                  if not os.path.exists(currentDB+"/."+tableName2.upper()):
                     print "Can not SELECT table "+ tableName2 + ", it does not exist"
                     continue 
                  alias2 = tokens.pop(0).upper() #get  table2 alias
            if len(tokens) >=1:
               command = (tokens.pop(0)).upper()
               if command == "WHERE":
                  whereSeen = True
                  whereAttrib, whereOperand, whereValue = extractInfoElements(tokens)


            #after we extract everything then we call the resepective update function to carry out the operation
            #but first input checking 
            if whereSeen:
               attributeIndex, attributeType, charNumber = getAttributeIndexAndType(whereAttrib, tableName)

  #----->             #CHECK IF WE ARE DOING A JOING OR A REGULAR SELECT
               if isJoin:
                  orderReversed = False
                  attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                  if attributeIndex == -1: # the first operand was no in the first table 
                     orderReversed = True
                     attributeIndex, attributeType, charNumber = getAttributeIndexAndType((whereAttrib.upper()).replace((alias2+'.').upper() , ''), tableName2.upper())
                     if attributeIndex == -1:
                        print "can not find "+ whereAttrib+" in any privided table"
                        continue
                  if orderReversed:
                     attributeIndex2, attributeType2, charNumber2 = getAttributeIndexAndType((whereValue.upper()).replace((alias1+'.').upper() , ''), tableName.upper())
                     if attributeIndex2 == -1:
                        print "can not find "+whereValue+" in any provided table"
                  if orderReversed: #will use whereValue since that is the right hand
                     #the following will innerjoin but first it will strip the table names of each 
                     outputTable = innerJoin(tableName,tableName2, (whereValue.upper()).replace((alias1+'.').upper() , ''), (whereAttrib.upper()).replace((alias2+'.').upper() , ''))
                  else: #will use whereValue since that is the right hand
                     #the following will innerjoin but first it will strip the table names of each 
                     outputTable = innerJoin(tableName,tableName2, (whereAttrib.upper()).replace((alias1+'.').upper() , ''), (whereValue.upper()).replace((alias2+'.').upper() , ''))
                  selectAllTable(outputTable)
               else:
                  if attributeIndex <= -1:
                     print "cant SELECT, attribute "+ whereAttrib.upper()+" not in table"
                     continue
                  if attributeType == "INT":
                     if  not isInt(whereValue):
                        print "cant SELECT table, in WHERE: INT expected"
                        continue
                  elif attributeType == "FLOAT":
                     if not isFloat(whereValue):
                        print "cant SELECT table, in WHERE: FLOAT expected"
                        continue
                  elif attributeType == "CHAR":
                     whereValue = whereValue.replace("'",'')
                     whereValue = whereValue.replace('"','')
                     setValue = padChar(charNumber)
                  elif attributeType == "VARCHAR":
                     whereValue = whereValue.replace("'",'')
                     whereValue = whereValue.replace('"','')
                  else:   
                     print "Cant SELECT, value of the WHERE command is of unknown type"
                     continue
                  selectFrom(tableName,attributeList, whereAttrib , whereOperand , whereValue , KeepResultTable = False)
            elif isJoin:
               print "can not JOIN without a WHERE"
               continue
            else:#if no where statement
               selectFrom(tableName,attributeList)
         else:
            print "Can not SELECT table "+ tableName + ", it does not exist" 
            continue


   # elif command == "TEST":
   #    #make sure that the atrib names in exist, check when taking the input before passing to functions
   #    selectAllTable(cartesianProduct("EMPLOYEE","SALES"))
   #    selectAllTable(innerJoin("EMPLOYEE","SALES", "ID", "EMPLOYEEID"))
   #    #dropTable(temp)
   else:
      print("Command "+command+" not valid")


filelist=glob.glob(currentDB+"/.TEMP_*")
for file in filelist:
   os.remove(file)
filelist=glob.glob(currentDB+"/TEMP_*")
for file in filelist:
   os.remove(file)
print("bye")

