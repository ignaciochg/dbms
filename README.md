# DBMS
#### Last Modified: nov 14 2018


Database Management System for CS457 written in Python 2.7

# Running the program
   - This python script should be run with Python 2.7 on Linux only  
   - The script was made for Ubuntu 18.04, should work on all Linux  


# Major Versions
## Version 1.01 
         -Ignacio Chamorro(22 September 2018)  
              Original Implementation of Design
              Can create tables and databases and select tables(only *)
## Version 2.01
         -Ignacio Chamorro(19 October 2018)  
              Can do operations on tables: insert, update, delete, select(updated)
## Version 3.01
         -Ignacio Chamorro(19 October 2018)  
              Can do inner joins and left inner joins
              Bug fixes

# Details Design of DBMS & File Design 
   Each version stacks on the next one.

   Notes:
   - this DBMS uses immediate materialization
   - the program takes care of almost all edge cases, hopefully i didn't miss one

## Notes on Version 1

- All the databases will be created in the working directory where the DBMS was launched from
- Each db will be its own folder in the working directory
- All the information of the db will be stored in side the db folder in the file ".db"
- First value of the .db file its the database real name, the way the user inputed
- All the tables will be stored in the database folder
- The .table_name files will contain table schema
- The table_name files will contain the records of the table

how the program works:
```
it is a main while loop that gets user input  
   it gets more input while it doest get the ; character  
   then it detects comment then it will ignore the comment  
   then it check if the command is EXIT  
   then it will tokenize the input  
      it will extract the command  
         if the command is CREATA  
            it will detect if we mean a table of database and do as described above  
         if the command is USE  
            then it check if it exists ans changes   
         if the commands is DROP  
            then it will delete the corresponding files  
         if it is SELECT  
            then it will check if it means *   
               if it does then call call the selectalltable function  
         if ALTER   
            then check if it is a table and add to the table schema  
         else just say that the command is not recognized   
```

## Notes on Version 2 (on added things)
   - removed old select, new select can select attributes and can use the where statement  

how it works:
```
in main loop get input:
   if INSERT
      validate input 
      then call insert insert function
   if UPDATE 
      validate input
      then call update function
   if DELETE
      validate input
      then call delete function
   if SELECT
      validate input, only FROM is mandatory as well as *, otherwise more parameters could be specified
      then call select
         select will process the request and call all the filtering functions (immediate materialization)
         then call selectAll function
         then delete the materialized functions
on EXIT; the program will delete all materialized tables that were for internal usage, except the ones that where renamed 

```
### New Functions added
#### Functions that get called after input validation:
```
insert()
removeFrom()
updateTable()
selectFrom()
```
#### Functions That get called by one on the above functions (AKA function helpers)
```
extractInfoElements() will return attribute name, operand, and value. input is list of strings(does not matter how they are split)
getAttributeIndexAndType() will return a index of the attribute and type, input attribute and table, will return -1 if no in table
extractValues() will return a list of values to insert, input is list of stings(split independent)
newRandomTableName() will generate a new name for temp table, guaranteed its unique
dropTable() will drop table
pi() will only return a table with the attributes specified
whereCheck() will check the where, input is all in string, this function will convert to relevant format to compare 
sigma() will filter table according to input
anyOfListInString() will check every value in a list if its in the string
```
## Notes on Version 3 (on added things)
	
   - since the select statement does not use any kind of regular expression and uses simple decision tree, the parser can only handle one operation at a given time (not nested, and one comparison in select), for example the select statement can not do a join and filtering in one operation.  
   - the system assumes only one comparison per select statement  
   - if doing join it is mandatory to have the table aliases (not mandatory in most dbms but enforced here since I decided the constrains of the system) and in the form of FROM TABLE1 ALIAS1, TABLE2 ALIAS2  	 
```
cartesianProduct() will do a Cartesian product of two tables
generateBlankRecord() will generate a blank record of th stable inputed, so it can be appended to the left table in left join
leftJoin() will do a left join of two tables
innerJoin() will do a inner join between two tables 
```

## Notes on Version 4 (on added things)
   
   - Transactions implemented  
      - begin transaction;   
      - commit;    
      - abort;    
   - When a transaction starts it will queue all the transactions, then when successfully commuted it will run them, only then the user will know if the commands where incorrect or not.
   - A transaction will lock the whole database until committed, multiple transaction can be possible but have to be committed in order otherwise the transaction can get aborted   
   - Bug found that does not allow to redirect a file to the dbms  
      - Therefore each line has to be pasted manually  
   - Bug found that multiple line commands do not work  
      - It is required to make multi line commands to single line commands  

### Functions Added

```
createDBLock(lockID) will create a lock and set the owner of the lock to the PID provided
deleteDBLock() will delete the lock
checkDBLockFile() check if there exist a lock 
getDBLockOwner() returns the owner of who ever owns th lock
myID() returns the process id of who calls it 

```


